import 'package:flutter/material.dart';

class ItemWidget extends StatefulWidget {
  final data;

  ItemWidget({
    Key? key,
    this.data,
  }) : super(key: key);
  @override
  _ItemWidgetState createState() => _ItemWidgetState();
}

class _ItemWidgetState extends State<ItemWidget> {
  // DateTime now = new DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3,
      margin: const EdgeInsets.all(8),
      child: Row(
        children: [
          const SizedBox(
            width: 5,
          ),
          CircleAvatar(
            backgroundImage: NetworkImage(widget.data['picture']),
            // child: Image.network(widget.data['picture']),
            radius: 25,
          ),
          const SizedBox(
            width: 20,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 20,
              ),
              Text('Name: ${widget.data['name']}'),
              Text('Email: ${widget.data['email']}'),
              Text('Age: ${widget.data['age']}'),
              const SizedBox(
                height: 20,
              ),
            ],
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'dart:convert' as convert;

import 'package:http/http.dart' as http;
import 'package:test_app1/item_widget.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Test App',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Random Users'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  var data = [];
  var newDataList = [];
  bool isLoadingProgress = false;

  @override
  void initState() {
    loadData();

    super.initState();
  }

  loadData() async {
    setState(() {
      isLoadingProgress = true;
    });
    var url = Uri.parse("https://randomuser.me/api/?results=25");
    var response = await http.get(url);
    if (response.statusCode == 200) {
      var jsonResponse =
          convert.jsonDecode(response.body) as Map<String, dynamic>;
      var result = jsonResponse['results'];
      for (int i = 0; i < result.length; i++) {
        data.add({
          'name': result[i]['name']['title'] +
              " " +
              result[i]['name']['first'] +
              " " +
              result[i]['name']['last'],
          'email': result[i]['email'],
          'age': result[i]['dob']['age'],
          'picture': result[i]['picture']['large']
        });
      }
      setState(() {
        isLoadingProgress = false;
      });
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
    setState(() {
      isLoadingProgress = false;
    });
    newDataList = List.from(data);
  }

  onItemChanged(String value) {
    setState(() {
      newDataList = data
          .where((e) =>
              e["name"].toString().toLowerCase().contains(value) ||
              e["age"].toString().contains(value) ||
              e["email"].toString().toLowerCase().contains(value.toLowerCase()))
          .toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(widget.title),
        ),
        body: isLoadingProgress
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : Column(
                children: [
                  Container(
                    padding:
                        const EdgeInsets.only(left: 10, right: 10, top: 10),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(
                        Radius.circular(2),
                      ),
                    ),
                    child: Material(
                      elevation: 2,
                      color: Colors.white,
                      child: TextField(
                        decoration: InputDecoration(
                            hintText: "Search with name, email & age",
                            contentPadding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 15),
                            hintStyle: TextStyle(
                                color: Colors.grey[400],
                                fontSize: 15,
                                fontWeight: FontWeight.w500),
                            border: InputBorder.none,
                            suffixIcon: Container(
                                margin: const EdgeInsets.only(
                                    right: 10, bottom: 8, top: 8),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(2),
                                  color: Colors.black,
                                ),
                                child: const Icon(
                                  Icons.search,
                                  size: 20,
                                  color: Colors.white,
                                ))),
                        onChanged: onItemChanged,
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemCount: newDataList.length,
                      itemBuilder: (ctx, index) {
                        return ItemWidget(
                          data: newDataList[index],
                          // searchData: newDataList[index],
                        );
                      },
                    ),
                  ),
                ],
              )

        // floatingActionButton: FloatingActionButton(
        //   onPressed: _incrementCounter,
        //   tooltip: 'Increment',
        //   child: const Icon(Icons.add),
        // ), // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}
